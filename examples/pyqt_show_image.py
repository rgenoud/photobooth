#!/usr/bin/env python

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QApplication, QWidget, QLabel, QStackedWidget
from picamera2.previews.qt import QGlPicamera2
from picamera2 import Picamera2
from PyQt5.QtGui import QPixmap
from gpiozero import Button

BTN_PIN = 24 # pin for the start button GPIO24 == pin 18

def on_button_clicked():
    """ Called on main button clicked """
    button.setEnabled(False)
    cfg = picam2.create_still_configuration()
    picam2.switch_mode_and_capture_file(cfg, "test.jpg", signal_function=qpicamera2.signal_done)

def capture_done(job):
    """ Called when capture is finished """
    result = picam2.wait(job)
    button.setEnabled(True)

def big_button_pressed():
    """ Called when big button is pushed """

    # POC stacked widget change
    nb_idx = stackedWidget.count()
    current_idx = stackedWidget.currentIndex()
    next_idx = current_idx + 1
    if next_idx >= nb_idx:
        next_idx = 0
    stackedWidget.setCurrentIndex(next_idx)

# Setup GPIOs
big_button = Button(BTN_PIN)
big_button.when_pressed = big_button_pressed

# Setup Camera
picam2 = Picamera2()
picam2.configure(picam2.create_preview_configuration())
picam2.start()

# Create Qt application
app = QApplication([])

# Camera widget
qpicamera2 = QGlPicamera2(picam2, width=1024, height=600, keep_ar=False)
qpicamera2.done_signal.connect(capture_done)

# Button unused for now
button = QPushButton("Click to capture JPEG")
button.clicked.connect(on_button_clicked)

# Image: Start camera
start_cam_widget = QLabel()
pixmap = QPixmap('intro.png')
start_cam_widget.setPixmap(pixmap)
start_cam_widget.setScaledContents(True)

# Image: instructions (strike your pose)
instruction_widget = QLabel()
pixmap = QPixmap('instructions.png')
instruction_widget.setPixmap(pixmap)
instruction_widget.setScaledContents(True)

# Image: processing
processing_widget = QLabel()
pixmap = QPixmap('processing.png')
processing_widget.setPixmap(pixmap)
processing_widget.setScaledContents(True)

# Image: finished
finished_widget = QLabel()
pixmap = QPixmap('finished2.png')
finished_widget.setPixmap(pixmap)
finished_widget.setScaledContents(True)

# Image: Uploading
uploading_widget = QLabel()
pixmap = QPixmap('uploading.png')
uploading_widget.setPixmap(pixmap)
uploading_widget.setScaledContents(True)

# Main widget
# We'll change its content with setCurrentIndex/setCurrentWidget
stackedWidget = QStackedWidget()
stackedWidget.addWidget(qpicamera2)
stackedWidget.addWidget(start_cam_widget)
stackedWidget.addWidget(instruction_widget)
stackedWidget.addWidget(processing_widget)
stackedWidget.addWidget(uploading_widget)
stackedWidget.addWidget(finished_widget)
stackedWidget.setCurrentWidget(start_cam_widget)

# Main layout
layout_v = QVBoxLayout()
layout_v.addWidget(stackedWidget)

# Main window setup
window = QWidget()
window.setWindowTitle("Photobooth So & Rico")
window.resize(1024, 600)
window.setLayout(layout_v)
window.showFullScreen()

# Qt main loop
app.exec()
