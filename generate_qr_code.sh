#!/bin/sh -e

head_txt="Retrouvez les photos en ligne :"
foot_txt="Mot de passe :"
bg_color="000000"
fg_color="d09742"
txt_sz=54
font="Liberation-Sans-Bold"
width=1024
height=600
out_file="res/qrcode.png"

usage() {
	printf "%s URL password\\n" "$0"
	printf "\\tGenerate %s with given URL and password\\n" "$out_file"
	printf "\\tEx: %s https://mycloud.mydomain.fr/s/xxxxxxxxxxxxxxx EQSQ2STjJJBPac\\n" "$0"
}

if [ $# -ne 2 ]; then
	usage
	exit 1
fi

url="$1"
pwd_txt="$2"

#convert -font "Liberation-Sans-Bold" -background "#000000" -size 1024x600 -fill "#d09742" -pointsize 54 -gravity North label:'Retrouvez les photos en ligne :' output.png && qiv output.png

head_height=$(( txt_sz * 2 ))
foot_height=$(( txt_sz * 2 ))
qr_height=$(( height - head_height - foot_height ))

head_img="$(mktemp photobooth.XXXXXXXXXX.png)"
foot_img="$(mktemp photobooth.XXXXXXXXXX.png)"
qr_small_img="$(mktemp photobooth.XXXXXXXXXX.png)"
qr_img="$(mktemp photobooth.XXXXXXXXXX.png)"

convert -font "$font" -background "#$bg_color" -size ${width}x${head_height} -fill "#$fg_color" -pointsize $txt_sz -gravity Center label:"$head_txt" "$head_img"
convert -font "$font" -background "#$bg_color" -size ${width}x${foot_height} -fill "#$fg_color" -pointsize $txt_sz -gravity Center label:"$foot_txt $pwd_txt" "$foot_img"
qrencode -s 10 --foreground="$fg_color" --background="$bg_color" -l H -o "$qr_small_img" "$url"
convert "$qr_small_img" -gravity center -background "#$bg_color" -extent ${width}x${qr_height} "$qr_img"

convert "$head_img" "$qr_img" "$foot_img" -append "$out_file"

rm -f "$head_img" "$foot_img" "$qr_small_img" "$qr_img"
