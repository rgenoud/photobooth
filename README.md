# Photomaton

Pour prendre des photos simplement pendant des évènements festifs

![démo de photomaton](img/photobooth_en_action.jpg)

## Pourquoi ce projet ?

Pour une cousinade, il nous fallait un système pour prendre des photos de tout
le monde et pouvoir les partager facilement.

Au lieu de louer un équipement, j'ai décidé d'en faire un, à base de raspberry-pi,
 d'un partage nexcloud et de pas mal de récup'.

![photo en pied](img/photobooth_en_pied.jpg)

## Quel est le matériel nécessaire ?

* Une raspberry PI ( ici une 3B+ )
* Une cameraPI v3 (cela fonctionne peut être avec une camera v2)
* Un écran hdmi (ici un 7" tacile, mais ce n'est pas nécessaire)
* Un bouton déclencheur
* Un petit circuit d'adaptation de tension pour le bouton (un transistor
2N2222 et une résistance de 10kΩ)
* Un interrupteur monostable pour éteindre la Rpi proprement
* Un interrupteur bistable pour passer en mode HDR
* Et un coffret maison pour tout installer dedans

Le bouton déclencheur peut se trouver [ici](https://boutique.semageek.com/fr/528-bouton-arcade-geant-100mm-avec-led-rouge-3009871889889.html)

![photo en pied](img/photobooth_interieur.jpg)

## Installation du logiciel sur la Rpi

Il suffit de cloner le dépôt, par exemple dans /home/pi.

L'exécutable photobooth.py sera alors dans le répertoire /home/pi/photobooth/

Les photos seront dans /home/pi/photobooth/pics par défaut

## Lancement au démarrage

Il suffit de lancer ces commandes:

```
mkdir -p /home/pi/.config/autostart/
cat > /home/pi/.config/autostart/photobooth.desktop << EOF
[Desktop Entry]
Type=Application
Version=1.0
Name=Photobooth
GenericName=Photobooth
Comment=Photobooth
Exec=/home/pi/dev/photobooth/photobooth.py
Icon=photobooth
Terminal=false
Type=Application
StartupNotify=false
EOF
```

## Synchronisation avec un nextcloud

Il faut tout d'abord créer un nouveau mot de passe d'application dans nextcloud/sécurité.

Puis créer le fichier contenant ces informations:

```
cat > /home/pi/.netrc <<EOF
machine mycloud.mydomain.fr
login monlogin
password p@ssw0rd
EOF
chmod 600 /home/pi/.netrc
```

Puis, en tant que root:

```
apt install nextcloud-desktop-cmd
cat > /etc/systemd/system/photobooth-synchro.service <<EOF
[Unit]
After=network.target

[Service]
ExecStart=nextcloudcmd -n /home/pi/photobooth/pics/ "https://mycloud.mydomain.fr/remote.php/webdav/Photos/party time"
User=pi
Group=pi
EOF

cat > /etc/systemd/system/photobooth-synchro.timer <<EOF
[Timer]
OnCalendar=*:0/5:0
Persistent=false

[Install]
WantedBy=timers.target
EOF

systemctl daemon-reload
systemctl enable photobooth-synchro.timer
systemctl start photobooth-synchro.timer
systemctl list-timers --all
```

En remplaçant bien sûr le chemin vers nextcloud.

## Création du qr-code

Pour créer l'image qrcode.png :

```
./generate_qr_code.sh https://mycloud.mydomain.fr/s/xxxxxxxxxxxxxxx P@ssw0rd
```

## Bouton d'extinction

Il est préférable d'éteindre proprement le photomaton pour éviter des erreurs
sur le système de fichier.

La GPIO d'arrêt de la raspberry PI est par défaut la GPIO 3 (pin 5).

Il faut alors faire en tant que root :

```
echo "dtoverlay=gpio-shutdown" >> /boot/config.txt
```

Et ajouter un bouton monostable entre la masse et la pin 5 pour commander
l'extinction du photomaton
