#!/bin/sh -e

# Go into the nextcould instante, create a folder and share it
# change those following links:
WEBDAV_URL="https://cloud.mycloud.fr/remote.php/webdav/Photobooth/my birthday party"
WEB_URL="https://cloud.mycloud.fr/s/DDTwMN6Q5Vbcve"
PASSWORD="VIpaNap28tlJxKi40kfr63HwFfU"

PICS_DIR=/home/pi/photobooth/pics
PICS_DIR_OLD="$PICS_DIR".old

sudo systemctl stop photobooth-synchro.timer

if [ -d "$PICS_DIR_OLD" ]; then
	rm -fr -- "$PICS_DIR_OLD"
fi

if [ -d "$PICS_DIR" ]; then
	mv "$PICS_DIR" "$PICS_DIR_OLD"
fi
mkdir -p "$PICS_DIR"

sudo sh -c "cat >/etc/systemd/system/photobooth-synchro.service << EOF
[Unit]
After=network.target

[Service]
ExecStart=nextcloudcmd -n /home/pi/photobooth/pics/ \"$WEBDAV_URL\"
User=pi
Group=pi
EOF
"

cd dev/photobooth
./generate_qr_code.sh "$WEB_URL" "$PASSWORD"
cp res/qrcode.png ../../photobooth/res/

