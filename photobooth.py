#!/usr/bin/env python

import signal
import os

from PyQt5.QtCore import (QTimer, pyqtSignal, pyqtSlot, QDateTime, Qt)
from PyQt5.QtWidgets import (QPushButton, QVBoxLayout, QApplication,
                             QLabel, QStackedWidget, QMainWindow)
from picamera2.previews.qt import QGlPicamera2
from picamera2 import Picamera2
from libcamera import (controls, Transform)
from PyQt5.QtGui import QPixmap
from gpiozero import (Button, LED, PWMLED)

################
# Configuration
################
# GPIOs
BTN_GPIO = 24 # pin for the start button GPIO24 == pin 18
LED_GPIO = 4 # pin for the LED GPIO4 == pin 7
HDR_GPIO = 2 # pin for the HDR option GPIO2 == pin 3

# Timings
PREP_DELAY = 2000 # number of ms at step 1 as users prep to have photo taken
PREVIEW_DELAY = 3000 # number of ms for the preview delay
FINISHED_DELAY = 1500 # number of ms for the finished slide
LAST_CALL_BEFORE_PHOTO_DELAY = 1000 # number of ms before taking the photo with the led blinking super-rapidly
SHOW_PHOTO_DELAY = 3000 # number of ms for showing the photo just taken
SHOT_NUMBER_SHOW_DELAY = 1000 # number of ms for showing the shot number slide
QRCODE_DELAY = 30000 # number os ms for showing the qrcode / start instruction

# File paths
PHOTOS_DIR = "/home/pi/photobooth/pics/"
RESOURCES_DIR = "/home/pi/photobooth/res/"

# Screen size
WIDTH = 1024
HEIGHT = 600

SHOTS_NB = 3 # Number of shots each time

class StackedWidget(QStackedWidget):
    """ Main widget for the Photobooth """
    def __init__(self, qpicamera2):
        super().__init__()

        self.qpicamera2 = qpicamera2

        # Image: Start camera / QRcode
        self.start_cam_widget = QLabel()
        self.intro_pixmap = QPixmap(RESOURCES_DIR + 'intro.png')
        self.qrcode_pixmap = QPixmap(RESOURCES_DIR + 'qrcode.png')
        self.start_cam_widget.setPixmap(self.intro_pixmap)
        self.start_cam_widget.setScaledContents(True)
        self.qrcode = False

        # Image: instructions (strike your pose)
        self.instruction_widget = QLabel()
        pixmap = QPixmap(RESOURCES_DIR + 'instructions.png')
        self.instruction_widget.setPixmap(pixmap)
        self.instruction_widget.setScaledContents(True)

        # Image: processing
        self.processing_widget = QLabel()
        pixmap = QPixmap(RESOURCES_DIR + 'processing.png')
        self.processing_widget.setPixmap(pixmap)
        self.processing_widget.setScaledContents(True)

        # Image: finished
        self.finished_widget = QLabel()
        pixmap = QPixmap(RESOURCES_DIR + 'finished2.png')
        self.finished_widget.setPixmap(pixmap)
        self.finished_widget.setScaledContents(True)

        # Image: Uploading
        self.uploading_widget = QLabel()
        pixmap = QPixmap(RESOURCES_DIR + 'uploading.png')
        self.uploading_widget.setPixmap(pixmap)
        self.uploading_widget.setScaledContents(True)

        # Image: shot number
        self.shot_nb_widget = QLabel()

        # Image: custom
        self.custom_widget = QLabel()

        self.addWidget(self.qpicamera2)
        self.addWidget(self.start_cam_widget)
        self.addWidget(self.instruction_widget)
        self.addWidget(self.processing_widget)
        self.addWidget(self.uploading_widget)
        self.addWidget(self.finished_widget)
        self.addWidget(self.shot_nb_widget)
        self.addWidget(self.custom_widget)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.change_start_pixmap)
        self.timer.start(QRCODE_DELAY)

    def start(self):
        """ Show the start slide """
        self.setCurrentWidget(self.start_cam_widget)

    def instructions(self):
        """ Show the instructions """
        self.setCurrentWidget(self.instruction_widget)

    def processing(self):
        """ Show the processing slide """
        self.setCurrentWidget(self.processing_widget)

    def uploading(self):
        """ Show the uploading slide """
        self.setCurrentWidget(self.uploading_widget)

    def finished(self):
        """ Show the last slide """
        self.setCurrentWidget(self.finished_widget)

    def camera(self):
        """ Show the camera """
        self.setCurrentWidget(self.qpicamera2)

    def custom(self, filename):
        """ Show a custom image """

        # Use the given file and scale it to the screen
        pixmap = QPixmap(filename)
        pixmap = pixmap.scaled(WIDTH, HEIGHT)
        self.custom_widget.setPixmap(pixmap)
        self.custom_widget.setScaledContents(True)

        self.setCurrentWidget(self.custom_widget)

    def shot(self, nb):
        """ Show the shot number slide """

        shot_nb_file = RESOURCES_DIR + "pose" + str(nb) + '.png'
        print(shot_nb_file)
        try:
            pixmap = QPixmap(shot_nb_file)
        except:
            print("Unable to open " + shot_nb_file)
        else:
            if not pixmap.isNull():
                pixmap = pixmap.scaled(WIDTH, HEIGHT)
                self.shot_nb_widget.setPixmap(pixmap)
                self.shot_nb_widget.setScaledContents(True)

                self.setCurrentWidget(self.shot_nb_widget)
            else:
                print("Unable to open " + shot_nb_file)

    def change_start_pixmap(self):
        if self.qrcode is True:
            self.start_cam_widget.setPixmap(self.intro_pixmap)
        else:
            self.start_cam_widget.setPixmap(self.qrcode_pixmap)
        self.qrcode = not self.qrcode

class MainWindow(QMainWindow):
    """ Main window for the Photobooth """

    big_button_signal = pyqtSignal(int)

    def __init__(self):
        super().__init__()

        # is there a session in progress ?
        self.in_progress = False

        # the last photoshot
        self.last_photo_filenames = list()

        # We can take several shots each time
        self.current_shot = 0

        # Setup big button
        self.big_button = Button(BTN_GPIO)
        self.big_button.when_pressed = self.big_button_pressed

        self.big_button_signal.connect(self.start_photo)

        # Setup Button LED
        self.led = PWMLED(pin = LED_GPIO, frequency=100)

        # Show a nice pulse
        self.led.pulse(fade_in_time=1, fade_out_time=1, n=None, background=True)

        # Setup HDR button
        self.hdr_button = Button(HDR_GPIO)
        if self.hdr_button.is_pressed:
            self.hdr_status = 1
        else:
            self.hdr_status = 0
        # There's nothing in the library to set HDR
        os.system("v4l2-ctl --set-ctrl wide_dynamic_range=" + str(self.hdr_status) +
                  " -d /dev/v4l-subdev0")

        # Setup Camera
        self.picam2 = Picamera2()
        preview_config = self.picam2.create_preview_configuration()
        # TODO: we should peek that from a video list in Picamera2.sensor_modes
        preview_config['size'] = ( 1820, 1024 )
        preview_config['transform'] = Transform(hflip=True)
        self.picam2.configure(preview_config)

        # Set continuous autofocus
        self.picam2.set_controls({"AfMode": controls.AfModeEnum.Continuous})

        # Camera widget
        self.qpicamera2 = QGlPicamera2(self.picam2, width=WIDTH, height=HEIGHT, keep_ar=False)
        self.qpicamera2.done_signal.connect(self.capture_done)

        # Main widget
        # We'll change its content with setCurrentIndex/setCurrentWidget
        self.stacked_widget = StackedWidget(self.qpicamera2)
        self.picam2.start()
        self.stacked_widget.start()

        self.setWindowTitle("Photobooth So & Rico")
        self.resize(WIDTH, HEIGHT)
        self.setCentralWidget(self.stacked_widget)
        self.showFullScreen()

    def big_button_pressed(self):
        """ Called when big button is pushed """
        # We can't start a timer here, because we are not in the
        # main_window thread, so we use a signal
        self.big_button_signal.emit(1)

    @pyqtSlot(int)
    def start_photo(self):
        """ Called after the big button is pushed """

        # prevent to push the button several times
        if self.in_progress is True:
            return
        self.in_progress = True

        self.current_shot = 1
        self.last_photo_filenames.clear()

        # show instructions
        self.stacked_widget.instructions()

        # Check if HDR mode has changed
        if self.hdr_button.is_pressed != bool(self.hdr_status):
            self.change_hdr_mode()
        else:
            self.prep_photo()

    def prep_photo(self):
        if SHOTS_NB == 1:
            # wait a little before showing the cam preview
            QTimer.singleShot(PREP_DELAY, self.preview_cam)
        else:
            # Show the shot number
            QTimer.singleShot(PREP_DELAY, self.show_shot_nb)

        # start blinking to get people ready
        self.led.blink(on_time=0.1, off_time=0.5)

    def change_hdr_mode(self):
        """ Setup the HDR mode """

        print("Setting HDR mode")
        if self.hdr_button.is_pressed:
            self.hdr_status = 1
        else:
            self.hdr_status = 0

        print("Stopping cam")
        try:
            self.picam2.stop()
        except:
            print("Unable to stop picam2")

        # wait a little before starting again camera
        QTimer.singleShot(3000, self.change_hdr_mode_step2)

    def change_hdr_mode_step2(self):
        """ Called after self.change_hdr_mode """

        # There's nothing in the library to set HDR
        os.system("v4l2-ctl --set-ctrl wide_dynamic_range=" + str(self.hdr_status) +
                  " -d /dev/v4l-subdev0")

        print("starting cam")
        self.picam2.start()
        self.prep_photo()

    def show_shot_nb(self):
        """ Show the shot number on a slide """

        self.stacked_widget.shot(self.current_shot)

        # wait a little before showing the cam preview
        QTimer.singleShot(SHOT_NUMBER_SHOW_DELAY, self.preview_cam)

    def preview_cam(self):
        """ Called when instruction have been shown """

        # Show the cam preview
        self.stacked_widget.camera()

        # Blink rapdily to warn people the photo will be taken soon
        self.led.blink(on_time=0.1, off_time=0.2)

        # wait for people to strike the pose
        QTimer.singleShot(PREVIEW_DELAY, self.before_shot)

    def before_shot(self):
        """ This is called just before taking the photo """

        # Blink even more rapidly to urge people when there's less than 1 sec left
        self.led.blink(on_time=0.1, off_time=0.1)

        # Take the photo just after that
        QTimer.singleShot(LAST_CALL_BEFORE_PHOTO_DELAY, self.take_shot)

    def take_shot(self):
        """ Take the photo """

        # create the configuration with a better definition
        cfg = self.picam2.create_still_configuration()
        now = QDateTime.currentDateTime()
        filename = PHOTOS_DIR + now.toString("yyyy-MM-dd_HH-mm-ss") + ".jpeg"

        # Save the photo filename to show it at the end
        self.last_photo_filenames.append(filename)

        # Last blink
        self.led.blink(on_time=1, off_time=0)

        # Take the picture !!
        self.picam2.switch_mode_and_capture_file(cfg, filename,
                                            signal_function=self.qpicamera2.signal_done)

    def capture_done(self, job):
        """ Called when capture is finished """

        # wait for the picture to complete
        self.picam2.wait(job)

        if self.current_shot >= SHOTS_NB:
            # Show the finished slide
            self.stacked_widget.finished()

            # Launch the timer do get back to the start screen
            QTimer.singleShot(FINISHED_DELAY, self.show_photo)
        else:
            self.current_shot += 1
            if SHOTS_NB == 1:
                # no need to show the shot nb is there's only one
                self.before_shot()
            else:
                # Show the shot number
                self.show_shot_nb()

    def show_photo(self):
        """ Show the photos just taken """

        filename = self.last_photo_filenames.pop(0)

        self.stacked_widget.custom(filename)

        if len(self.last_photo_filenames) > 0:
            # Launch the timer for next photo
            QTimer.singleShot(SHOW_PHOTO_DELAY, self.show_photo)
        else:
            # Launch the timer do get back to the start screen
            QTimer.singleShot(SHOW_PHOTO_DELAY, self.restart)

    def restart(self):
        """ Start back at the begining """

        # unlock the big button
        self.in_progress = False

        # show the start slide
        self.stacked_widget.start()

        # pulse like Kit
        self.led.pulse(fade_in_time=1, fade_out_time=1, n=None, background=True)

if __name__ == "__main__":
    app = QApplication([])

    # hide cursor
    app.setOverrideCursor(Qt.BlankCursor)

    # Main window setup
    main_window = MainWindow()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app.exec()
